<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Form Penjadwalan Dosen</title>
  </head>
  <style>
  .container{
    background: grey;
  }
  </style>  
  <body>
  <div class="container border border-secondary">
  <div class="row align-items-start">
        <form action="hasil.php" method="post">
        <center>
        <h2>FORM PENJADWALAN DOSEN</h2>
        </center>
        <div class="row">
            <label for="input_nama_dosen" class="col-sm-2 col-form-label">Nama</label>
            <div class="form-mb-20">
              <input type="text" class="form-control" id="input_nama_dosen" name="input_nama_dosen" >
            </div><br>
            <label for="input_NIP_dosen" class="col-sm-2 col-form-label">NIP</label>
            <div class="form-mb-20">
              <input type="text" class="form-control" id="input_NIP_dosen" name="input_NIP_dosen" >
            </div><br>
            <label for="input_fakultas" class="col-sm-2 col-form-label">Fakultas</label><br>
            <div class="form-mb-1">
            <select class="form-select" id="input_fakultas" name="input_fakultas" aria-label="Default select example">
                   <option selected></option>
                   <option value="FTK">FTK</option>
                   <option value="FIP">FIP</option>
                   <option value="FE">FE</option>
                   <option value="FOK">FOK</option>
                   <option value="FBS">FBS</option>
                   <option value="FMIPA">FMIPA</option>
                   <option value="FK">FK</option>
             </select>
             </div>
             <label for="input_prodi" class="col-sm-2 col-form-label">Prodi</label>
             <div class="form-mb-1">
                <input type="text" class="form-control" id="input_prodi" name="input_prodi">
             </div><br>
             <label for="input_kelas" class="col-sm-2 col-form-label">Nama Kelas</label>
             <div class="form-mb-1">
                <input type="text" class="form-control" id="input_kelas" name="input_kelas">
             </div><br>
             <label for="input_jadwal" class="col-sm-2 col-form-label">Jadwal</label>
             <div class="form-mb-1">
                <input type="date" class="form-control" id="input_jadwal" name="input_jadwal">
             </div><br>
             <label for="input_matakuliah" class="col-sm-2 col-form-label">Matakuliah</label>
             <div class="form-mb-1">
               <input type="text" class="form-control" id="input_matakuliah" name="input_matakuliah">
             </div>
             <div class="d-grid gap-2"><br>
             <button type="submit" class="btn btn-primary">Buat Jadwal</button>
             </div><br>
             <div class="d-grid gap-4">
             <button type="reset" class="btn btn-danger" >Reset</button>
             </div>
        </form>
  </body>
  </html>